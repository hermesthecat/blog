const   mongoose    =   require("mongoose"),
        express     =   require("express"),
        passport    =   require("passport"),
        LocalStrategy = require("passport-local"),
        expressSession = require("express-session"),
        bodyParser  =   require("body-parser"),
        User        =   require("./models/userModel"),
        app         =   express();

// route ayarlamalari
const   indexRoutes =   require("./routes/indexRoutes");
        adminRoutes =   require("./routes/adminRoutes");
        blogRoutes =   require("./routes/blogRoutes");

// app ayarlamalari
mongoose.connect("mongodb+srv://abdullahazad:cf8S3ckXAhNgrIDt@cluster0.aj3gq.mongodb.net/BlogApp?retryWrites=true&w=majority")
app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended:true}));

// passport ayarlamalari
app.use(require("express-session")({
    secret: "HermesTheCat",
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// kullanici bilgisini tum route ile paylasalim
app.use((req, res, next)=>{
    res.locals.currentUser=req.user;
    next();
});

//routes kullanimlari
app.use(indexRoutes);
app.use(adminRoutes);
app.use(blogRoutes);

const   server      =   app.listen(3000, (err)=>{
        if(err){
            console.log(err); // server hata verirse console'a yazdiriyoruz.
        }
        console.log('http server baslatildi: ', server.address().port);
});