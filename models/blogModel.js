const     mongoose            = require("mongoose")

const blogSchema              = new mongoose.Schema({
    baslik:     {type:String, required: "can not be empty"},
    ozet:       {type:String, required: "can not be empty"},
    ozetResim:  {type:String, required: "can not be empty"},
    blogyazi:   {type:String, required: "can not be empty"}
    tarih:      {type: Date, default: Date.now}
})

module.exports = mongoose.model("Blog", blogSchema);