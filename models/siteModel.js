const     mongoose            = require("mongoose")


const siteSchema              = new mongoose.Schema({
    homeImage:  {type:String, required: "can not be empty"},
    aboutImage:  {type:String, required: "can not be empty"},
    aboutText:  {type:String, required: "can not be empty"},
    contactImage:  {type:String, required: "can not be empty"},
    contactText:  {type:String, required: "can not be empty"}
})

module.exports = mongoose.model("Site", siteSchema);