const   express     =       require('express'),
        router      =       express.Router();

router.get("/admin/new-blog", (req, res)=>{
    res.render('blog/newBlog');
});
        
router.get("/admin/settings", (req, res)=>{
    res.render('admin/admin');
});

module.exports = router;