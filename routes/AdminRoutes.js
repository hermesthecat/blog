const   express     =       require('express'),
        User        =       require('../models/userModel');  
        passport    =       require('passport');
        router      =       express.Router();

let     adminActions = [
    {
        actionId:1,
        actionName: "changeHomeImage",
        DisplayName: "Change Home Image"
    },
    {
        actionId:2,
        actionName: "changeAboutImage",
        DisplayName: "Change About Image"
    },
    {
        actionId:3,
        actionName: "changeAboutText",
        DisplayName: "Change About Text"
    },
    {
        actionId:4,
        actionName: "addNewBlog",
        DisplayName: "Add New Blog"
    },
    {
        actionId:5,
        actionName: "ListAllBlog",
        DisplayName: "List All Blog"
    }
];

router.get("/admin", (req, res)=> {
    res.render("admin/admin", {adminActions:adminActions})
});

router.get("/login", (req, res)=>{
    res.render('admin/signin');
});

router.post("/login", passport.authenticate("local",
        {
            successRedirect: "/admin",
            failureRedirect: "/login"
        }), (req, res)=>{});

router.get("/signup", (req, res)=>{
    res.render('admin/signup');
});

router.post("/signup", (req, res)=>{
    let newUser = new User({username: req.body.username});
    User.register(newUser, req.body.password, (err, user)=>{
        if(err){
            res.send("kayıt hatalı");
        }
        passport.authenticate("local") (req, res, ()=>{
            res.redirect("/login");
        });
    });
});

router.get("/cikis", (req, res)=>{
    req.logout();
    res.redirect("/");
});

module.exports = router;