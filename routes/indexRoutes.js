const   express     =       require('express'),
        router      =       express.Router();

let     data        = [
        {
            postTitle       : "birinci makale",
            postSubTitle    : "birinci makalenin içeriği burada yer alacak",
            postImage       : "https://mk0ghacksnety2pjrgh8.kinstacdn.com/wp-content/uploads/2021/03/microsoft-edge-sync-linux.png"
        },
        {
            postTitle       : "ikinci makale",
            postSubTitle    : "ikinci makalenin içeriği burada yer alacak",
            postImage       : "https://mk0ghacksnety2pjrgh8.kinstacdn.com/wp-content/uploads/2021/03/microsoft-edge-sync-linux.png"
        },
        {
            postTitle       : "ucuncu makale",
            postSubTitle    : "ucuncu makalenin içeriği burada yer alacak",
            postImage       : "https://mk0ghacksnety2pjrgh8.kinstacdn.com/wp-content/uploads/2021/03/microsoft-edge-sync-linux.png"
        }
];

router.get("/", (req, res)=>{
    res.render('home', {data : data});
});

router.get("/about", (req, res)=>{
    res.render('about');
});

router.get("/resume", (req, res)=>{
    res.render('resume');
});

router.get("/contact", (req, res)=>{
    res.render('contact');
});

module.exports = router;
